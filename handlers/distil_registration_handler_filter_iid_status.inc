<?php


/**
* @file
* Definition of distil_registration_handler_filter_iid_status.
*/

/**
* Filter handler for iid statuses.
*
* @ingroup views_filter_handlers
*/
class distil_registration_handler_filter_iid_status extends views_handler_filter_many_to_one {
  function get_value_options() {
    $this->value_options = ['unsure' => 'unsure', 'blacklist' => 'blacklist', 'whitelist' => 'whitelist'];
  }

}
