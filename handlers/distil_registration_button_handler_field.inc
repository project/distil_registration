<?php
/**
 * @file
 *   Views field handler for basic Distl Registration fields.
 */

/**
 * Views field handler for basic Distil Registration fields.
 *
 * The only thing we're doing here is making sure the field_alias
 * gets set properly, and that none of the sql-specific query functionality
 * gets called.
 */
class distil_registration_button_handler_field extends views_handler_field {
    function query() {
    }

    /**
     * Render the field.
     *
     * @param $values
     *   The values retrieved from the database.
     */
    function render($values) {
        $form_name = 'distil_registration_status_button';
        $fingerprint = $values->fingerprint;
        $iid_status = $values->iid_status;
        $elements = drupal_get_form($form_name, $fingerprint, $iid_status);
        return drupal_render($elements);
    }
}
