<?php
/**
 * @file
 *   Views query plugin for Distil Registration
 */

/**
 * Views query plugin for the Distil Registration IID'S.
 */
class distil_registration_plugin_query extends views_plugin_query_default {

    /*
     * A simple array of order by clauses.
     */
    var $orderby = [];

    /**
     * Generate a query and a countquery from all of the information supplied
     * to the object.
     *
     * @param $get_count
     *   Provide a countquery if this is true, otherwise provide a normal query.
     */
    function query($get_count = FALSE) {
        // Check query distinct value.
        if (empty($this->no_distinct) && $this->distinct && !empty($this->fields)) {
            if ($this->pure_distinct === FALSE) {
                $base_field_alias = $this->add_field($this->base_table, $this->base_field);
                $this->add_groupby($base_field_alias);
            }
            $distinct = TRUE;
        }

        /**
         * An optimized count query includes just the base field instead of all the fields.
         * Determine if this query qualifies by checking for a groupby or distinct.
         */
        $fields_array = $this->fields;
        if ($get_count && !$this->groupby) {
            foreach ($fields_array as $field) {
                if (!empty($field['distinct']) || !empty($field['function'])) {
                    $this->get_count_optimized = FALSE;
                    break;
                }
            }
        }
        else {
            $this->get_count_optimized = FALSE;
        }
        if (!isset($this->get_count_optimized)) {
            $this->get_count_optimized = TRUE;
        }

        $options = [];
        $target = 'default';
        $key = 'default';
        // Detect an external database and set the
        if (isset($this->view->base_database)) {
            $key = $this->view->base_database;
        }

        // Set the slave target if the slave option is set
        if (!empty($this->options['slave'])) {
            $target = 'slave';
        }

        // Go ahead and build the query.
        // db_select doesn't support to specify the key, so use getConnection directly.
        $subquery = db_select('field_data_field_distil_reg_iid', 'fdfdri');
        $subquery->leftJoin('distil_registration_iid_status', 'dris', 'fdfdri.field_distil_reg_iid_value = dris.distil_iid');
        $subquery->leftJoin('users', 'u', 'fdfdri.entity_id = u.uid');
        $subquery->leftJoin('users_roles', 'ur', 'ur.uid = u.uid');
        $subquery->leftJoin('role', 'r', 'r.rid = ur.rid');
        $subquery->leftJoin('field_data_field_websites', 'fdfw', 'fdfw.entity_id = u.uid');
        $subquery->leftJoin('field_data_field_bio', 'fdfb', 'fdfb.entity_id = u.uid');
        $subquery->leftJoin('field_data_field_organizations', 'fdfo', 'fdfo.entity_id = u.uid');
        $subquery->leftJoin('field_data_field_organization_name', 'fdfon', 'fdfon.entity_id = fdfo.field_organizations_value');
        $subquery->leftJoin('field_data_field_job_title', 'fdfjt', 'fdfjt.entity_id = fdfon.entity_id');
        // TODO add zid as leftjoin subquery --Needs Review
       // $subquery->leftJoin('field_data_field_reg_zid_fingerprint', 'fdfzf', 'fdfzf.entity_id = fdfon.entity_id');
        $subquery->leftJoin('field_data_field_distil_reg_zid', 'fdfdrz', 'fdfdrz.entity_id = fdfdri.entity_id');
        $subquery->leftJoin('node', 'n', 'n.uid = u.uid AND n.type = \'forum\'');
        // TODO add zid field as z_fingerprint --Needs Review
        $subquery->addField('fdfdrz', 'field_distil_reg_zid_value', 'zid_fingerprint');
        $subquery->addField('fdfdri', 'field_distil_reg_iid_value', 'fingerprint');
        $subquery->addField('dris', 'status', 'iid_status');
        $subquery->fields('u', ['uid', 'name', 'status', 'created']);
        $subquery->addExpression('GROUP_CONCAT(DISTINCT r.name)', 'Roles');
        $subquery->addExpression('GROUP_CONCAT(DISTINCT fdfw.field_websites_url)', '`WebsiteURLs`');
        $subquery->addExpression('GROUP_CONCAT(DISTINCT fdfon.field_organization_name_value)', 'OrgNames');
        $subquery->addExpression('GROUP_CONCAT(DISTINCT fdfjt.field_job_title_value)', 'JobTitles');
        $subquery->addExpression('GROUP_CONCAT(DISTINCT n.title)', 'ForumPostTitles');
        $subquery->addExpression('GROUP_CONCAT(DISTINCT fdfb.field_bio_value)', 'Bio');
        $subquery->groupBy('u.uid');

        $query = db_select($subquery, 'udata');
        $query->addExpression('COUNT(udata.Fingerprint)', '`fingerprints`');
        $query->addExpression('GROUP_CONCAT(DISTINCT udata.status)', 'status');
        $query->addExpression('GROUP_CONCAT(DISTINCT udata.Roles SEPARATOR \'|\')', 'roles');
        $query->addExpression('GROUP_CONCAT(DISTINCT udata.zid_fingerprint SEPARATOR \' \')', 'zid_fingerprint');
        $query->addExpression('GROUP_CONCAT(udata.uid SEPARATOR \' \')', 'uids');
        $query->addExpression('GROUP_CONCAT(udata.name SEPARATOR \' \')', 'names');
        $query->addExpression('GROUP_CONCAT(DISTINCT udata.`WebsiteURLs`)', 'website_urls');
        $query->addExpression('GROUP_CONCAT(DISTINCT udata.`ForumPostTitles` SEPARATOR \';\r\n\')', 'forum_post_titles');
        $query->addExpression('GROUP_CONCAT(DISTINCT udata.`OrgNames`)', 'org_names');
        $query->addExpression('FROM_UNIXTIME(MIN(udata.created))', 'first_seen');
        $query->addExpression('FROM_UNIXTIME(MAX(udata.created))', 'last_seen');
        $query->addExpression('(MAX(udata.created) - MIN(udata.created))', 'fingerprint_age');
        $query->addField('udata', 'fingerprint');
        // Add z_fingerprint
        $query->addField('udata', 'iid_status');
        $query->groupBy('udata.fingerprint');
        $query->havingCondition('fingerprints', '1', '>');

        // Add the tags added to the view itself.
        foreach ($this->tags as $tag) {
            $query->addTag($tag);
        }

        if (!empty($distinct)) {
            $query->distinct();
        }

        $joins = $where = $having = $orderby = $groupby = '';
        $fields = $distinct = [];


        if (!$this->get_count_optimized) {
            // we only add the orderby if we're not counting.
            if ($this->orderby) {
                foreach ($this->orderby as $order) {
                    if ($order['field'] == 'rand_') {
                        $query->orderRandom();
                    }
                    else {
                        $query->orderBy($order['field'], $order['direction']);
                    }
                }
            }
        }

        if (!empty($this->where) && $condition = $this->build_condition('where')) {
            $query->condition($condition);
        }

        // Add a query comment.
        if (!empty($this->options['query_comment'])) {
            $query->comment($this->options['query_comment']);
        }

        // Add the query tags.
        if (!empty($this->options['query_tags'])) {
            foreach ($this->options['query_tags'] as $tag) {
                $query->addTag($tag);
            }
        }

        // Add all query substitutions as metadata.
        $query->addMetaData('views_substitutions', module_invoke_all('views_query_substitutions', $this));

        if (!$get_count) {
            if (!empty($this->limit) || !empty($this->offset)) {
                // We can't have an offset without a limit, so provide a very large limit
                // instead.
                $limit = intval(!empty($this->limit) ? $this->limit : 999999);
                $offset = intval(!empty($this->offset) ? $this->offset : 0);
                $query->range($offset, $limit);
            }
        }

        return $query;
    }

    function ensure_table($table, $relationship = NULL, $join = NULL) {
        return 'udata';
    }
}
//    /**
//     * DBTNG for DISTIL IID user sql query
//     *
//     * @param int $items_per_page
//     * @param int $page
//     * @param bool $count
//     *
//     * @return
//     */
//    private function distil_registration_iid_report($items_per_page = 10, $page = 0, $count = FALSE) {
//        $startitem = $items_per_page * $page;
//        // drupal_page_is_cacheable(FALSE);
//        $query = db_select('field_data_field_distil_reg_iid', 'fdfdri');
//        $query->leftJoin('distil_registration_iid_status', 'dris', 'fdfdri.field_distil_reg_iid_value = dris.distil_iid');
//        $query->leftJoin('users', 'u', 'fdfdri.entity_id = u.uid');
//        $query->leftJoin('users_roles', 'ur', 'ur.uid = u.uid');
//        $query->leftJoin('role', 'r', 'r.rid = ur.rid');
//        $query->leftJoin('field_data_field_websites', 'fdfw', 'fdfw.entity_id = u.uid');
//        $query->leftJoin('field_data_field_bio', 'fdfb', 'fdfb.entity_id = u.uid');
//        $query->leftJoin('field_data_field_organizations', 'fdfo', 'fdfo.entity_id = u.uid');
//        $query->leftJoin('field_data_field_organization_name', 'fdfon', 'fdfon.entity_id = fdfo.field_organizations_value');
//        $query->leftJoin('field_data_field_job_title', 'fdfjt', 'fdfjt.entity_id = fdfon.entity_id');
//        $query->leftJoin('node', 'n', 'n.uid = u.uid AND n.type = \'forum\'');
//        $query->addField('fdfdri', 'field_distil_reg_iid_value', 'fingerprint');
//        $query->addField('dris', 'status', 'iid_status' );
//        $query->fields('u', array('uid', 'name', 'status', 'created'));
//        $query->addExpression('GROUP_CONCAT(DISTINCT r.name)', 'Roles');
//        $query->addExpression('GROUP_CONCAT(DISTINCT fdfw.field_websites_url)', '`WebsiteURLs`');
//        $query->addExpression('GROUP_CONCAT(DISTINCT fdfon.field_organization_name_value)', 'OrgNames');
//        $query->addExpression('GROUP_CONCAT(DISTINCT fdfjt.field_job_title_value)', 'JobTitles');
//        $query->addExpression('GROUP_CONCAT(DISTINCT n.title)', 'ForumPostTitles');
//        $query->addExpression('GROUP_CONCAT(DISTINCT fdfb.field_bio_value)', 'Bio');
//        $query->groupBy('u.uid');
//
//        $select = db_select($query, 'udata');
//        $select->addExpression('COUNT(udata.Fingerprint)', '`fingerprints`');
//        $select->addExpression('GROUP_CONCAT(DISTINCT udata.status)', 'status');
//        $select->addExpression('GROUP_CONCAT(DISTINCT udata.Roles SEPARATOR \'|\')', 'roles');
//        $select->addExpression('GROUP_CONCAT(udata.uid SEPARATOR \' \')', 'uids' );
//        $select->addExpression('GROUP_CONCAT(udata.name SEPARATOR \' \')', 'names');
//        $select->addExpression('GROUP_CONCAT(DISTINCT udata.`WebsiteURLs`)', 'website_urls');
//        $select->addExpression('GROUP_CONCAT(DISTINCT udata.`ForumPostTitles` SEPARATOR \';\r\n\')', 'forum_post_titles');
//        $select->addExpression('GROUP_CONCAT(DISTINCT udata.`OrgNames`)', 'org_names');
//        $select->addExpression('FROM_UNIXTIME(MIN(udata.created))', 'first_seen');
//        $select->addExpression('FROM_UNIXTIME(MAX(udata.created))', 'last_seen');
//        $select->addExpression('(MAX(udata.created) - MIN(udata.created))', 'fingerprint_age');
//        $select->addField('udata', 'fingerprint');
//        $select->addField('udata', 'iid_status');
//        $select->groupBy('udata.fingerprint');
//        $select->havingCondition('fingerprints', '1', '>');
//
//        if ($count) {
//            $distil_iid_report = $select->countQuery()->execute()->fetchField();
//        }
//        else {
//            if ($this->orderby) {
//                foreach ($this->orderby as $order) {
//                    $select->orderBy($order['field'], $order['direction']);
//                }
//            }
//
//                foreach ($this->where as $where) {
//                    foreach ($where['conditions'] as $condition) {
//                        $select->condition($condition['field'], $condition['value'], $condition['operator']);
//                    }
//                }
//            $select->range($startitem, $items_per_page);
//            $distil_iid_report = $select->execute()->fetchAll();
//        }
//        return $distil_iid_report;
//    }
//}
