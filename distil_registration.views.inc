<?php
/**
 * Implementation of hook_views_plugins().
 */
function distil_registration_views_plugins()
{
    $plugin = array();
    $plugin['query']['distil_registration_plugin_query'] = array(
        'title' => t('Distil Registration Query'),
        'help' => t('Distil Registration query object.'),
        'handler' => 'distil_registration_plugin_query',
    );
    return $plugin;
}

/**
 * Implementation of hook_views_data().
 */
function distil_registration_views_data()
{
    $data = array();

    // Base data
    $data['distil_registration']['table']['group'] = t('Distil Registration Data');
    $data['distil_registration']['table']['join']['distil_registration'] = [];
    $data['distil_registration']['table']['base'] = array(
        'title' => t('Distil Registration Data'),
        'help' => t('Distil Registration Data'),
        'query class' => 'distil_registration_plugin_query'
    );
    $data['distil_registration']['fingerprint'] = array(
        'title' => t('D_IID Fingerprint'),
        'help' => t('The D_IID fingerprint of the user in question.'),
        'field' => array(
            'handler' => 'distil_registration_handler_field',
            'click sortable' => TRUE,
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
            'allow empty' => TRUE,
        ),
    );
    $data['distil_registration']['fingerprints'] = array(
        'title' => t('Fingerprint Count'),
        'help' => t('The number of users sharing this fingerprint'),
        'field' => array(
            'handler' => 'distil_registration_handler_field',
            'click sortable' => TRUE,
        ),
    );
    $data['distil_registration']['status'] = array(
        'title' => t('UID Statuses'),
        'help' => t('List of statuses for this UID\'s matching this Fingerprint'),
        'field' => array(
            'handler' => 'distil_registration_handler_field',
            'click sortable' => TRUE,
        ),
    );
    $data['distil_registration']['roles'] = array(
        'title' => t('Roles'),
        'help' => t('List of all Roles shared by this fingerprint'),
        'field' => array(
            'handler' => 'distil_registration_handler_field',
            'click sortable' => TRUE,
        ),
    );
    $data['distil_registration']['first_seen'] = array(
        'title' => t('First Seen'),
        'help' => t('The first time an account was created with this D_IID'),
        'field' => array(
            'handler' => 'distil_registration_handler_field',
            'click sortable' => TRUE,
        ),
    );
    $data['distil_registration']['last_seen'] = array(
        'title' => t('Last Seen'),
        'help' => t('Date of most recent account on this D_IID'),
        'field' => array(
            'handler' => 'distil_registration_handler_field',
            'click sortable' => TRUE,
        ),
    );
    $data['distil_registration']['fingerprint_age'] = array(
      'title' => t('Fingerprint Age'),
      'help' => t('Time in seconds between first and last uid for this fingerprint'),
      'field' => array(
        'handler' => 'distil_registration_handler_field',
        'click sortable' => TRUE,
      ),
    );
    $data['distil_registration']['uids'] = array(
        'title' => t('uids'),
        'help' => t('The uids shared by this fingerprint'),
        'field' => array(
            'handler' => 'distil_registration_handler_field',
            'click sortable' => TRUE,
        ),
    );
    $data['distil_registration']['org_names'] = array(
        'title' => t('Organizations'),
        'help' => t('Organization names associated with this fingerprint.'),
        'field' => array(
            'handler' => 'distil_registration_handler_field',
            'click sortable' => TRUE,
        ),
    );
    $data['distil_registration']['job_titles'] = array(
        'title' => t('Job Titles'),
        'help' => t('Reported job titles.'),
        'field' => array(
            'handler' => 'distil_registration_handler_field',
            'click sortable' => TRUE,
        ),
    );
    $data['distil_registration']['forum_post_titles'] = array(
        'title' => t('Forum Titles'),
        'help' => t('Titles of published forum posts.'),
        'field' => array(
            'handler' => 'distil_registration_handler_field',
            'click sortable' => TRUE,
        ),
    );
    $data['distil_registration']['bio'] = array(
        'title' => t('Bios'),
        'help' => t('The bios associated with this fingerprint'),
        'field' => array(
            'handler' => 'distil_registration_handler_field',
            'click sortable' => TRUE,
        ),
    );
    $data['distil_registration']['names'] = array(
        'title' => t('Usernames'),
        'help' => t('The usernames for this fingerprint'),
        'field' => array(
            'handler' => 'distil_registration_handler_field',
            'click sortable' => TRUE,
        ),
    );
    $data['distil_registration']['website_urls'] = array(
        'title' => t('URLs'),
        'help' => t('Urls associated with this fingerprint'),
        'field' => array(
            'handler' => 'distil_registration_handler_field',
        ),
    );
    // TODO expose zid as a field --Needs Review
    $data['distil_registration']['zid_fingerprint'] = array(
        'title' => t('D_ZID Fingerprint'),
        'help' => t('The D_ZID fingerprint of the user in question.'),
        'field' => array(
            'handler' => 'distil_registration_handler_field',
            'click sortable' => TRUE,
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
            'allow empty' => TRUE,
        ),
    );

    $data['distil_registration']['iid_status'] = array(
        'title' => t('IID Status'),
        'help' => t('Status of a given IID WL/BL/Unknown'),
        'field' => array(
            'handler' => 'distil_registration_handler_field',
            'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'distil_registration_handler_filter_iid_status',
          'allow empty' => TRUE,
        ),
    );
    // IID form button
    $data['distil_registration']['manage_button'] = array(
        'title' => t('Manage IID Status'),
        'help' => t('Toggles between whitelist, blacklist, and unsure'),
        'field' => array(
            'handler' => 'distil_registration_button_handler_field',
        ),
    );
    return $data;
}
